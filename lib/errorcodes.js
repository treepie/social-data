exports.SUCCESS = 200;
exports.ERROR = 400;
exports.CONTROLLER_NOT_REGISTERED = 11000;

var errormessages = {};
errormessages[this.SUCCESS] = 'Success';
errormessages[this.ERROR]='Some error occurred.';
errormessages[this.CONTROLLER_NOT_REGISTERED]='Controller Not Registered';

exports.getErrorMessage = function(errorId){
    return errormessages[errorId];
}

